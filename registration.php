<?php
/**
 * Lyonscg_BundleTierPrice
 *
 * Module registration.
 *
 * @author    Alexey Savchenko <asavchenko@lyonscg.com>
 * @copyright 2018 Lyons Consulting Group (www.lyonscg.com)
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Lyonscg_BundleTierPrice',
    __DIR__
);
