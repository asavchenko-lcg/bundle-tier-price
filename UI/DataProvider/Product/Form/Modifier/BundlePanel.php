<?php
/**
 * Overrides default BundlePanel in order to show Fixed Price option for tier Price
 *
 *
 * @author    Alexey Savchenko <asavchenko@lyonscg.com>
 * @copyright 2017 Lyons Consulting Group (www.lyonscg.com)
 */

namespace Lyonscg\BundleTierPrice\UI\DataProvider\Product\Form\Modifier;

/**
 * Class BundlePanel
 * @package Lyonscg\BundleTierPrice\UI\DataProvider\Product\Form\Modifier
 */
class BundlePanel extends \Magento\Bundle\Ui\DataProvider\Product\Form\Modifier\BundlePanel
{
    /**
     * @param array $meta
     *
     * @return array
     */
    public function modifyMeta(array $meta)
    {
        $priceOptions = $this->getTierPriceOptions($meta);
        $meta         = parent::modifyMeta($meta);

        return $priceOptions ? $this->addFixedTierPrice($meta, $priceOptions) : $meta;
    }


    /**
     * @param array $meta
     *
     * @return mixed|null
     */
    protected function getTierPriceOptions($meta)
    {
        return $this->arrayManager->get($this->getPricePath($meta), $meta);
    }

    /**
     * @param array $meta
     *
     * @return string
     */
    protected function getPricePath($meta)
    {
        $tierPricePath = $this->arrayManager->findPath(
            \Magento\Catalog\Api\Data\ProductAttributeInterface::CODE_TIER_PRICE,
            $meta,
            null,
            'children'
        );
        $pricePath     = $this->arrayManager->findPath(
            \Magento\Catalog\Api\Data\ProductAttributeInterface::CODE_TIER_PRICE_FIELD_PRICE,
            $meta,
            $tierPricePath
        );

        return $this->arrayManager->slicePath($pricePath, 0, -1).'/value_type/arguments/data/options';
    }

    /**
     * @param array $meta
     * @param array $priceOptions
     *
     * @return array
     */
    protected function addFixedTierPrice($meta, $priceOptions)
    {
        return $this->arrayManager->merge(
            $this->arrayManager->slicePath($this->getPricePath($meta), 0, -1),
            $meta,
            ['options' => $priceOptions]
        );
    }
}