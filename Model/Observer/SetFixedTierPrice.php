<?php
/**
 * Observer for the event catalog_product_get_final_pric
 *
 * Sets fixed tier price for bundle items
 *
 * @author    Alexey Savchenko asavchenko@lyonscg.com
 * @copyright 2017 Lyons Consulting Group (www.lyonscg.com)
 */

namespace Lyonscg\BundleTierPrice\Model\Observer;

use Magento\Catalog\Model\Product\Type;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class SetFixedTierPrice
 * @package Lyonscg\BundleTierPrice\Model\Observer
 */
class SetFixedTierPrice implements ObserverInterface
{
    /** @var Type  */
    protected $productType;

    /**
     * SetFixedTierPrice constructor.
     */
    public function __construct(Type $productType)
    {
        $this->productType = $productType;
    }

    /**
     * Execute
     *
     * @param EventObserver $observer
     * @return $this
     */
    public function execute(EventObserver $observer)
    {
        $product = $observer->getEvent()->getProduct();
        $qty = $observer->getEvent()->getQty();
        if ($product->getTypeId() !== Type::TYPE_BUNDLE) {
            return $this;
        }
        $tierPrice = $product->getTierPrice($qty);
        if (floatval($tierPrice) > 0) {// it's tier price in % (default behaviour)
            return $this;
        }
        $tierPrice = $this->getPriceModel()->getTierPrice($qty, $product);
        if (floatval($tierPrice) > 0) {// fixed tier price is used
            $product->setData('final_price', $tierPrice);
        }

        return $this;
    }

     /**
     * We need simple product behaviour
     *
     * @return \Magento\Catalog\Model\Product\Type\Price
     */
    public function getPriceModel()
    {
        return $this->productType->priceFactory(Type::TYPE_SIMPLE);
    }
}